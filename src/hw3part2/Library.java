package hw3part2;

import java.util.ArrayList;
import java.util.Random;

public class Library {
    ArrayList<Book> books = new ArrayList<Book>();
    ArrayList<Visitor> visitors = new ArrayList<Visitor>();

    public void addBook(Book book) {
        boolean isFind = false;
        for (Book current : books) {
            if (current.equals(book)) {
                isFind = true;
                break;
            }
        }
        if (!isFind) {
            book.setStatus(true);
            books.add(book);
        }
    }

    public Book getRandomBook() {
        Random random = new Random();
        if (books.size() == 0)
            return null;
        return books.get(random.nextInt(books.size()));
    }

    public ArrayList<Book> getAllBooks() {
        return books;
    }

    public void deleteBook(Book book) {
        int index = -1;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).equals(book)) {
                index = i;
                break;
            }
        }
        if (index != -1 && books.get(index).isStatus()) {
            books.remove(index);
        }
    }

    public Book getBook(String name) {
        for (Book current : books) {
            if (current.getName().equals(name)) {
                return current;
            }
        }
        return null;
    }

    public ArrayList<Book> getBooksByAuthor(String author) {
        ArrayList<Book> foundBooks = new ArrayList<Book>();

        for (Book current : books) {
            if (current.getAuthor().equals(author)) {
                foundBooks.add(current);
            }
        }
        return foundBooks;
    }

    public boolean borrowBookByTitle(Visitor visitor, String name) {
        for (Book current : books) {
            if (current.getName().equals(name) && current.isStatus()) {
                if (visitor.getBorrowedBook() == null) {
                    if (visitor.getId() == null) {
                        visitors.add(visitor);
                        visitor.setId(visitors.size() - 1);
                        current.setStatus(false);
                        visitor.setBorrowedBook(current);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean returnBookFromVisitor(Integer id, Book book) {
        if (id != null && id >= 0 && id < visitors.size()) {
            if (visitors.get(id).getBorrowedBook().equals(book)) {
                book.setStatus(true);
                visitors.get(id).setBorrowedBook(null);
                return true;
            }
        }
        return false;
    }
}
