package hw3part2;

import hw3part2.Book;
import hw3part2.Library;
import hw3part2.Visitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

class Main {
    static void initLib(Library lib) {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            lib.addBook(new Book("book" + i, "author" + random.nextInt(4)));
        }
    }

    public static void main(String[] args) {

        Library lib = new Library();

        initLib(lib);

        for (Book book : lib.getAllBooks()) {
            System.out.println(book.getName() + "\t" + book.getAuthor());
        }

        int n = 5;
        ArrayList<Visitor> visitors = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            visitors.add(new Visitor("visitor " + i));
        }

        for (Visitor v : visitors) {
            String title = lib.getRandomBook().getName();
            System.out.print(v.getName() + " try get book " + title + ": ");
            System.out.println(lib.borrowBookByTitle(v, title));
        }
        Book testBook = lib.getBook("book5");
        //System.out.println(testBook.getName()); getName возвращает null, т.к. такой книги нет

        ArrayList<Book> testlist = new ArrayList<>();
        testlist = lib.getBooksByAuthor("author1");

        System.out.println("Книги, которые написал author1: ");
        for (int i = 0; i < testlist.size(); i++) {
            System.out.println(testlist.get(i).getName());
        }
        lib.addBook(new Book("book10","author10"));

        System.out.println("Список книг в библиотеке:");
        for (Book book : lib.getAllBooks()) {
            System.out.println(book.getName() + "\t" + book.getAuthor());
        }
        Visitor testvisitor = new Visitor("Eugen");
        lib.borrowBookByTitle(testvisitor, "book10");

        testBook = testvisitor.getBorrowedBook();
        System.out.println("Книга, которую одолжил " + testvisitor.getName());
        System.out.println(testBook.getName());

        lib.returnBookFromVisitor(testvisitor.getId(),testvisitor.getBorrowedBook());


        System.out.println("Статус последней книги в каталоге " + lib.books.get(lib.books.size()-1).isStatus());

        lib.deleteBook(lib.books.get(lib.books.size()-1));

        System.out.println("Список книг в библиотеке после удаления book10:");

        for (Book book : lib.getAllBooks()) {
            System.out.println(book.getName() + "\t" + book.getAuthor());
        }
    }

}