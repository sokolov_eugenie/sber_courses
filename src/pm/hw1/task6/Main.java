package pm.hw1.task6;

/**
 * Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
 * очень зол и ему придется написать свои проверки, а также кидать исключения,
 * если проверка провалилась. Помогите Пете написать класс FormValidator со
 * статическими методами проверки. На вход всем методам подается String str.
 * a. public void checkName(String str) — длина имени должна быть от 2 до 20
 * символов, первая буква заглавная.
 * b. public void checkBirthdate(String str) — дата рождения должна быть не
 * раньше 01.01.1900 и не позже текущей даты.
 * c. public void checkGender(String str) — пол должен корректно матчится в
 * enum Gender, хранящий Male и Female значения.
 * d. public void checkHeight(String str) — рост должен быть положительным
 * числом и корректно конвертироваться в double.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            String name = scanner.nextLine();
            FormValidator.checkName(name);
        } catch (FormValidator.IncorrectFormatException e) {
            System.out.println(e.getMessage());
        }

        try {
            String birthdate = scanner.nextLine();
            FormValidator.checkBirthdate(birthdate);
        } catch (FormValidator.IncorrectFormatException e) {
            System.out.println(e.getMessage());
        }

        try {
            String gender = scanner.nextLine();
            FormValidator.checkGender(gender);
        } catch (FormValidator.IncorrectFormatException e) {
            System.out.println(e.getMessage());
        }

        try {
            String height = scanner.nextLine();
            FormValidator.checkHeight(height);
        } catch (FormValidator.IncorrectFormatException e) {
            System.out.println(e.getMessage());
        }
    }
}


