package pm.hw1.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n % 2 == 0) {
            MyEvenNumber myEvenNumber = new MyEvenNumber(n);
        } else {
            throw new RuntimeException("Нечётное число!");
        }

//        try {
//            MyEvenNumber myEvenNumber = new MyEvenNumber(n);
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            return;
//        }

        MyPrimeNumber myPrimeNumber = new MyPrimeNumber(n);
    }
}
