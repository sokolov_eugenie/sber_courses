package pm.hw1.task4;

public class MyEvenNumber {
    private int evenNumber = 0;

    public int getEvenNumber() {
        return evenNumber;
    }

    MyEvenNumber(int n) {
        if (n % 2 != 0) {
            throw new IllegalArgumentException("Для создания инстанса MyEvenNumber число должно быть четным!");
        }
        evenNumber = n;
    }

}
