package pm.hw1.task1;

public class Main {
    public static void main(String[] args) {

        try {
            throw new MyCheckedException();
        } catch (MyCheckedException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            methodWithMyCheckedException();
        } catch (MyCheckedException e){
            System.out.println(e.getMessage());
        }
    }
    public static void methodWithMyCheckedException()
            throws MyCheckedException
    {
        throw new MyCheckedException("MyCheckedException from methodWithMyCheckedException()");
    }
}

