package hw3part3.task1;

public abstract class Animal {
    protected final void eat() {
        System.out.println("Ест");
    }

    protected final void sleep() {
        System.out.println("Спит");
    }
}

