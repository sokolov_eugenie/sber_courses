package hw3part3.task1;

public class Main {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Eagle eagle = new Eagle();
        Goldfish goldfish = new Goldfish();
        Dolphin dolphin = new Dolphin();

        System.out.println("Летучие мыши: ");
        bat.fly();
        bat.wayOfBirth();
        bat.sleep();
        bat.eat();
        System.out.println();

        System.out.println("Орлы: ");
        eagle.fly();
        eagle.wayOfBirth();
        eagle.sleep();
        eagle.eat();
        System.out.println();

        System.out.println("Золотые рыбки: ");
        goldfish.swim();
        goldfish.wayOfBirth();
        goldfish.sleep();
        goldfish.eat();
        System.out.println();

        System.out.println("Дельфины: ");
        dolphin.swim();
        dolphin.wayOfBirth();
        dolphin.sleep();
        dolphin.eat();
    }
}

