package hw3part3.task2;

public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        Stool stool = new Stool();
        Table table = new Table();
        System.out.println("Ремонтирует ли цех данную мебель? " + bestCarpenterEver.canRepair(table));
        System.out.println("Ремонтирует ли цех данную мебель? " + bestCarpenterEver.canRepair(stool));
        System.out.println("Ремонтирует ли цех данную мебель? " + bestCarpenterEver.canRepair(null));

    }
}

