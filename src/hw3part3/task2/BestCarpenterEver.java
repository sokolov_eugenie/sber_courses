package hw3part3.task2;

public class BestCarpenterEver {
    public boolean canRepair(Furniture furniture) {
        if (furniture == null) {
            return false;
        }
        return furniture instanceof Stool;
    }
}

