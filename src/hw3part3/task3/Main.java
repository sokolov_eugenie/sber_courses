package hw3part3.task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        Matrix matrix = new Matrix(n, m);
        matrix.display();
    }
}

