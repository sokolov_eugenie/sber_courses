package hw3part3.task3;

import java.util.ArrayList;

public class Matrix {
    ArrayList<ArrayList<Integer>> matrix;

    public Matrix(int columns, int row) {
        this.matrix = new ArrayList<>();
        for (int i = 0; i < row; i++) {
            matrix.add(new ArrayList<>());
            for (int j = 0; j < columns; j++) {
                matrix.get(i).add(i + j);
            }

        }
    }

    public void display() {
        for (ArrayList<Integer> integers : matrix) {
            for (int j = 0; j < integers.size(); j++) {
                if (j < integers.size() - 1) System.out.print(integers.get(j) + " ");
                else System.out.print(integers.get(j));
            }
            System.out.println();
        }
    }
}

