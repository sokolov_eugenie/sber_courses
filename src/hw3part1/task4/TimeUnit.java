package hw3part1.task4;

public class TimeUnit {
    private int hour;
    private int minutes;
    private int seconds;

    public TimeUnit() {
        this.hour = 0;
        this.minutes = 0;
        this.seconds = 0;
    }

    public TimeUnit(int hour) {
        if (hour < 0 || hour > 23) {
            throw new IllegalArgumentException("Введены некорректные значения!");
        }
        this.hour = hour;
        this.minutes = 0;
        this.seconds = 0;
    }

    public TimeUnit(int hour, int minutes) {
        if (hour < 0 || hour > 23 || minutes < 0 || minutes > 59) {
            throw new IllegalArgumentException("Введены некорректные значения!");
        }
        this.hour = hour;
        this.minutes = minutes;
        this.seconds = 0;
    }

    public TimeUnit(int hour, int minutes, int seconds) {
        if (hour < 0 || hour > 23 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59) {
            throw new IllegalArgumentException("Введены некорректные значения!");
        }
        this.hour = hour;
        this.minutes = minutes;
        this.seconds = seconds;
    }


    public void addTime(int hour, int minutes, int seconds) {
        if (hour < 0 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59) {
            throw new IllegalArgumentException("Введены некорректные значения!");
        }
        if (this.seconds + seconds >= 60) {
            this.minutes = this.minutes + (this.seconds + seconds) / 60;
            this.seconds = (this.seconds + seconds) % 60;
        } else this.seconds = this.seconds + seconds;
        if (this.minutes + minutes >= 60) {
            this.hour = this.hour + (this.minutes + minutes) / 60;
            this.minutes = (this.minutes + minutes) % 60;
        } else this.minutes = this.minutes + minutes;
        if (this.hour + hour >= 24) {
            this.hour = (this.hour + hour) % 24;
        } else this.hour = this.hour + hour;
    }

    public void showTime24hours() {
        System.out.println((hour < 10 ? "0" + hour : hour) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds));
    }

    public void showTime12hours() {
        if (hour / 12 == 0)
            System.out.println((hour < 10 ? "0" + hour : hour) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds) + " AM");
        else
            System.out.println((hour < 10 ? "0" + (hour-12) : (hour-12)) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds) + " PM");
    }

}

