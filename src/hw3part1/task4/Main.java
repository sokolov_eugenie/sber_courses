package hw3part1.task4;

public class Main {
    public static void main(String[] args) {
        TimeUnit timeUnit = new TimeUnit(10, 15, 45);
        TimeUnit timeUnit2 = new TimeUnit(22, 25);
        timeUnit.showTime24hours();

        timeUnit.addTime(5, 10, 15);
        timeUnit.showTime24hours();
        timeUnit2.showTime12hours();
        timeUnit2.addTime(1, 25, 55);
        timeUnit2.showTime24hours();
    }

}
