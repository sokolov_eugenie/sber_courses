package hw3part1.task1;
    class Cat {
    // Приватный метод, выводящий "Sleep"
    private void sleep() {
        System.out.println("Sleep");
    }
    // Приватный метод, выводящий "Meow"
    private void meow() {
        System.out.println("Meow");
    }
    // Приватный метод, выводящий "Eat"
    private void eat() {
        System.out.println("Eat");
    }
    //публичный метод, вызывающий один из приватных случайным образом
    public void status() {
        int random = (int) (Math.random() * 3);

        if (random == 0) {
            sleep();
        } else if (random == 1) {
            meow();
        } else {
            eat();
        }
    }
}
public class Task1 {
    public static void main(String[] args) {
        Cat barsik = new Cat(); // объект типа Cat

        barsik.status();
        barsik.status();
        barsik.status();// вызов метода status() несколько раз
        barsik.status();
        barsik.status();
    }
}

