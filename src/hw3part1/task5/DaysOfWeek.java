package hw3part1.task5;

public class DaysOfWeek {
    private byte dayNumber;
    private String dayName;

    public DaysOfWeek(byte newDayNumber, String newDayName) {
        this.dayNumber = newDayNumber;
        this.dayName = newDayName;
    }

    public byte getDayNumber() {
        return dayNumber;
    }

    public String getDayName() {
        return dayName;
    }
}
