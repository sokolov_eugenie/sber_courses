package hw3part1.task5;

public class Main {
    public static void main(String[] args) {
        DaysOfWeek[] week = new DaysOfWeek[7];
        week[0] = new DaysOfWeek((byte) 1, "Monday");
        week[1] = new DaysOfWeek((byte) 2, "Tuesday");
        week[2] = new DaysOfWeek((byte) 3, "Wednesday");
        week[3] = new DaysOfWeek((byte) 4, "Thursday");
        week[4] = new DaysOfWeek((byte) 5, "Friday");
        week[5] = new DaysOfWeek((byte) 6, "Saturday");
        week[6] = new DaysOfWeek((byte) 7, "Sunday");
        displayArray(week);
    }
    public static void displayArray(DaysOfWeek[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i].getDayNumber() + " " + arr[i].getDayName());
        }
    }
}
