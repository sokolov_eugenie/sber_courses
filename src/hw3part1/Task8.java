package hw3part1;

import java.util.jar.Attributes;

public class Task8 {
    public static void main(String[] args) {
        // Задаем 3 курса
        Atm atmOne = new Atm(0.015, 68.3);// банкомат1
        Atm atmTwo = new Atm(0.05, 100); // банкомат2

        System.out.println(atmOne.convertRublesToDollars(68.3));//перевд рублей в доллары в банкомате1
        System.out.println(atmOne.convertDollarsToRubles(1));//перевод долларов в рубли в банкомате2

        System.out.println(atmTwo.convertRublesToDollars(200));
        System.out.println(atmTwo.convertDollarsToRubles(450));

        // Выводим количество курсов валют
        System.out.println(Atm.printCount());
    }
}

class Atm {
    private static int count; // количество созданных экземпляров
    private double courseRublesToDollars;
    private  double courseDollarsToRubles;

    Atm(double courseRublesToDollars, double courseDollarsToRubles){
        if (courseRublesToDollars > 0 && courseDollarsToRubles > 0){
            this.courseRublesToDollars = courseRublesToDollars;
            this.courseDollarsToRubles = courseDollarsToRubles;
            count++;
        }else {
            throw new IllegalArgumentException("Ввведены некорректные значения");
        }
    }
    public double convertRublesToDollars(double rubles){
        return rubles * courseRublesToDollars;
    }
    public double convertDollarsToRubles(double dollars){
        return dollars * courseDollarsToRubles;
    }
    public static int printCount(){
        return count;
    }
}
