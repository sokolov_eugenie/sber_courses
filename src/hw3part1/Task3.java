package hw3part1;

import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {

        StudentService studentService = new StudentService();

        // Объявляем массив студентов
        Student[] student = new Student[3];
        student[0] = new Student();
        student[1] = new Student();
        student[2] = new Student();

        student[0].setName("Eugenie");
        student[0].setSurname("Sokolov");
        student[0].setGrades(new int[] {2, 3, 4, 5, 4}); // инфа про первого студента

        student[1].setName("Dmitry");
        student[1].setSurname("Morozov");
        student[1].setGrades(new int[] {5, 4, 3, 2, 3, 4, 5, 5});  // инфа про второго студента

        student[2].setName("Vladislav");
        student[2].setSurname("Levichev");
        student[2].setGrades(new int[] {5, 5, 2, 2, 3, 4, 4, 3}); // инфа про третьего студента

        System.out.println(studentService.bestStudent(student)); // студент с наивысшим средним баллом

        studentService.sortBySurname(student); // сортируем в алфавитном порядке и выводим
    }
}

class StudentService {
    public String bestStudent(Student[] students){
        double max = 0;
        int indx = -1;

        // поиск максимального среднего балла
        for (int i = 0; i < students.length; i++){
            if (students[i].averageGrades() > max){
                max = students[i].averageGrades();
                indx = i;
            }
        }
        return "Студент с наивысшим средним баллом: " + students[indx].getName() + " " + students[indx].getSurname();
    }

    public void sortBySurname(Student[] students){
        String[] surname = new String[students.length];
        for (int i = 0; i < students.length; i++) {
            surname[i] = students[i].getSurname();
        }
        Arrays.sort(surname);
        System.out.println("Список студентов по алфавиту: ");
        int number = 1;
        for (String s : surname) {
            System.out.println(number + "." + s);
            ++number;
        }
    }
}

