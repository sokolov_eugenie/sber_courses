package hw3part1;

import java.util.Arrays;

class Student {
    private String name;
    private String surname;
    private final int[] grades = new int[10]; // последние 10 оценок студента
    private int numberOfGrades; // кол-во оценок

    public void setName(String name){
        this.name = name;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }
    public void setGrades(int[] grade){
        for (int j : grade) addGrades(j);
    }
    public String getName(){
        return name;
    }
    public String getSurname(){
        return surname;
    }
    public String getGrades(){
        int[] result = new int[numberOfGrades]; // массив, который будет хранить полученные оценки
        for (int i = 0; i < grades.length; i++){
            if (grades[grades.length - i - 1] != 0)
                result[numberOfGrades - i - 1] = grades[grades.length - i - 1];
        }
        return Arrays.toString(result);
    }
    public void addGrades(int grade){

        for (int i = 1; i < grades.length; i++) // сдвиг влево
            grades[i - 1] = grades[i];

        grades[grades.length - 1] = grade;// добавить новую в конец
        numberOfGrades++;
    }
    public double averageGrades(){
        double sum = 0;
        for (int i : grades)
            sum += i;
        return sum / numberOfGrades;
    }
}
public class Task2{
    public static void main(String[] args) {
        Student student = new Student();

        student.setName("Eugenie");
        student.setSurname("Sokolov");

        student.setGrades(new int[]{2, 3, 4, 5, 4, 3, 2}); // последние 7 оценок

        student.addGrades(3);// +1 оценка

        System.out.println(student.getName()); // выводим имя студента
        System.out.println(student.getSurname()); // выводим фамилию студента
        System.out.println(student.getGrades()); // выводим последние оценки студента
        System.out.println(student.averageGrades()); // выводим средний балл студента
    }
}

