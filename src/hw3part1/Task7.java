package hw3part1;
public class Task7 {
    public static void main(String[] args) {

        System.out.println(TriangleChecker.check(1.5, 1.5, 1.5));
        System.out.println(TriangleChecker.check(2.3, 3.3, 1.3));
        System.out.println(TriangleChecker.check(-1, 0.1, 1.9));
        System.out.println(TriangleChecker.check(4.5, 4.5, 5.5));
    }
}

class TriangleChecker {
    public static boolean check(double side1, double side2, double side3){
        if (side1 <= 0 || side2 <= 0 || side3 <= 0)
            return false;
        if (side1 + side2 > side3 && side2 + side3 > side1 && side1 + side3 > side2)
            return true;
        return false;
    }
}
